# FUSIBLE_Groupe2

## Description
The Fuse Project focuses on the development of fuse models and an associated test bench.
The primary goal is to create accurate and versatile models for fuses, enhancing the simulation capabilities for various scenarios.

Authors:    
Patrick Sarkis     
Vanessa Samara     
Marc Rahal

The code is organized in the ProjectWithMaterial package which contains:

- 3 subpackages: Niveau1, Niveau2, Niveau3 
- For each subpackage: its simulation model TestFuse, its main component Fuse and a multitest model TestPlusieurs.
- A model that counts the fusion time: TempsDeFusion
- A subpackage Materials that contains MaterialProp of the fuse.

The project is hosted in the public repository https://gitlab-student.centralesupelec.fr/patrick.sarkis/fusible_groupe2

## Simulation
To run the simulation for each level :

P.S: It's important to note that the Fuse class should not be executed directly, as it represents an electrical dipole.

Open ProjectWithMaterial.mo    
In OMEdit,  
Open TestFuse of each level       
Run Simulation      
Display for N1: in fuseN1 => i and etat 
(you can change the value of the current Itest)

Display for N2: in fuseN2 => T and i   
(you can change the value of the current Itest/ or change the section, length of the fuse, material, convection coefficient)
                in temps de fusion you can see its value cteTdf and see it on the curve tempsdefusion.

Display for N3: in fuseN3 => T and i   


Display for TestPlusieurs: 

N1:Itest and etat (you can change the values of the currents Itest from the table tab_Itest)

N2:Itest and Tdf  from tab_Itest and  tab_tdf (you can change the values of the currents Itest from the table tab_Itest)

N3:Itest and Tdf  from tab_Itest and  tab_tdf (you can change the values of the currents Itest from the table tab_Itest)

P.S: You may need to change the simulation time
 
You can also see the fuse caracteristic curve by clicking on New Array Parametric Plot window
then right click +shift on the tab_Itest then right click on tab_tdf and finally click on the play button.

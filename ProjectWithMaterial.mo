package ProjectWithMaterial "This package has a Materials subpackage which contains a library of MaterialProp records which can be used for the Fuse.mat parameter"
  package Niveau1
    model TestFuseN1 "Test the change of material of the fuse"
      extends Modelica.Icons.Example;
      parameter Current Itest = 1 "fuse test current";
      FuseN1 fuse annotation(
        Placement(transformation(origin = {-2, -24}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Electrical.Analog.Basic.Ground ground annotation(
        Placement(transformation(origin = {72, 28}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Electrical.Analog.Basic.Resistor resistor(R = rampVoltage.V/Itest) annotation(
        Placement(transformation(origin = {-48, 8}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Sources.RampVoltage rampVoltage(V = 1, duration = 1) annotation(
        Placement(transformation(origin = {0, 40}, extent = {{-10, -10}, {10, 10}})));
    equation
      connect(resistor.n, fuse.p) annotation(
        Line(points = {{-48, -2}, {-48, -24}, {-12, -24}}, color = {0, 0, 255}));
      connect(rampVoltage.p, resistor.p) annotation(
        Line(points = {{-10, 40}, {-48, 40}, {-48, 18}}, color = {0, 0, 255}));
      connect(rampVoltage.n, fuse.n) annotation(
        Line(points = {{10, 40}, {46, 40}, {46, -24}, {8, -24}}, color = {0, 0, 255}));
      connect(ground.p, rampVoltage.n) annotation(
        Line(points = {{72, 38}, {10, 38}, {10, 40}}, color = {0, 0, 255}));
      annotation(
        Diagram(coordinateSystem(initialScale = 0.1)),
        experiment(StartTime = 0, StopTime = 5, Tolerance = 1e-06, Interval = 0.01));
    end TestFuseN1;

    model FuseN1 "Fuse model Niveau 1"
      // Niveau 1: Le fusible se comporte comme un simple interrupteur
      //Le fusible laisse passer le courant tant qu’il est suffisamment faible (inférieur à un courant      nominal In réglable) et le bloque de façon permanent sinon.
      // dipole électrique
      extends Modelica.Electrical.Analog.Interfaces.OnePort;
      //Fusible état fermé
      final parameter Resistance Ron = 1e-5 "Ron";
      //Fusible état ouvert
      final parameter Resistance Roff = 1e5 "Roff";
      parameter Current In = 1 "Courant nominal";
      // Variable d'état
      Modelica.Blocks.Interfaces.BooleanOutput etat annotation(
        Placement(transformation(origin = {0, 104}, extent = {{-10, -10}, {10, 10}}, rotation = 90), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    initial equation
      etat = true;
// Initialement fermé
    equation
// Comparaison du courant traversant le fusible à son courant Nominal
      when i > In then
        etat = false;
// Fusible état ouvert
      end when;
// Loi d'Ohm
      v = if etat then Ron*i else Roff*i;
      annotation(
        Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}), Text(origin = {1, -1}, extent = {{-101, 41}, {99, -39}}, textString = "%mat"), Text(origin = {0, 69}, textColor = {0, 0, 255}, extent = {{-100, 29}, {100, -29}}, textString = "%name"), Line(origin = {-110, 0}, points = {{-10, 0}, {10, 0}}), Line(origin = {110, 0}, points = {{-10, 0}, {10, 0}}), Rectangle(extent = {{-90, 12}, {90, -12}})}));
    end FuseN1;
    model TestPlusieurs "Banc de test de courants"
      extends Modelica.Icons.Example;
      parameter Integer n = 10 "Nombre de tests desirés"; 
      Boolean tab_etats[n] "Tableau des différents états";
      parameter Real tab_Itest[n] = {0.1, 0.5, 1, 2, 3, 4, 5, 10, 20, 50} "Tableau des différents courants de test";
      TestFuseN1 testFuse[n](Itest = tab_Itest) annotation(
        Placement(transformation(extent = {{-16, -16}, {16, 16}})));
    equation
      tab_etats = testFuse.fuse.etat;
      annotation(
        experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-06, Interval = 0.02));
    end TestPlusieurs;
  end Niveau1;

  package Niveau2
    model TestFuseN2
      extends Modelica.Icons.Example;
      parameter Current Itest = 10 "fuse test current";
      Modelica.Electrical.Analog.Basic.Resistor resistor(R = constantVoltage.V/Itest) annotation(
        Placement(transformation(origin = {-48, 6}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      FuseN2 fuseN2(mat = Materials.silver) annotation(
        Placement(transformation(origin = {-1.77636e-15, -54}, extent = {{-28, -28}, {28, 28}})));
      Modelica.Electrical.Analog.Basic.Ground ground annotation(
        Placement(transformation(origin = {72, 26}, extent = {{-10, -10}, {10, 10}})));
      Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = 100) annotation(
        Placement(transformation(origin = {0, 46}, extent = {{-10, -10}, {10, 10}})));
      TempsDeFusion tempsDeFusion annotation(
        Placement(transformation(origin = {68, -52}, extent = {{-10, -10}, {10, 10}})));
    equation
      connect(fuseN2.p, resistor.n) annotation(
        Line(points = {{-28, -54}, {-48, -54}, {-48, -4}}, color = {0, 0, 255}));
      connect(constantVoltage.p, resistor.p) annotation(
        Line(points = {{-10, 46}, {-48, 46}, {-48, 16}}, color = {0, 0, 255}));
      connect(constantVoltage.n, ground.p) annotation(
        Line(points = {{10, 46}, {72, 46}, {72, 36}}, color = {0, 0, 255}));
      connect(fuseN2.n, constantVoltage.n) annotation(
        Line(points = {{28, -54}, {50, -54}, {50, 46}, {10, 46}}, color = {0, 0, 255}));
      connect(fuseN2.etat, tempsDeFusion.etat) annotation(
        Line(points = {{0, -26}, {0, -14}, {68, -14}, {68, -42}}, color = {255, 0, 255}));
      annotation(
        experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-06, Interval = 0.02),
        Diagram);
    end TestFuseN2;

    model FuseN2 "Fuse model Niveau 2"
      // Niveau 2: modèle dynamique physique, avec effet de retard temporel, avec fil homogène dont la température uniforme.
      // Dipole électrique
      extends Modelica.Electrical.Analog.Interfaces.OnePort;
      // Matériel utilisé
      parameter Materials.MaterialProp mat = Materials.aluminium "fuse material";
      Modelica.Units.SI.Temperature T(displayUnit = "K");
      // Constantes
      constant Temperature T0(displayUnit = "K") = 20 "Température de l'air ambiant";
      // Paramètres du fusible
      parameter Length l = 0.03 "Longueur du fil";
      final parameter Area S = Modelica.Constants.pi*(d/2)^2 "Section du fil";
      parameter Length d(displayUnit = "mm") = 64e-6 "Diamètre du fil";
      // Paramètres des équations différentielles
      parameter CoefficientOfHeatTransfer h = 10 "Coefficient de convection";
      parameter Real taux = (d*mat.rho*mat.Cm)/(4*h) "Constante dans l'equation diff";
      Modelica.Units.SI.Temperature T2(displayUnit = "K") "Température en fonction de I";
      //Fusible etat ferme
      parameter Resistance Ron = l/(S*mat.sigma) "Ron";
      //Fusible etat ouvert
      parameter Resistance Roff = 1e5 "Roff";
      // Variable d'état du fusible
      Modelica.Blocks.Interfaces.BooleanOutput etat annotation(
        Placement(transformation(origin = {4, 98}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {0, 100}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
    initial equation
      etat = true;
      T = T0;
    equation
// Equation représentant les variations de la température du fil du fusible en fonction du temps
      T2 = T0 + (4*(i^2))/(mat.sigma*h*(Modelica.Constants.pi^2)*(d^3));
      der(T) + T/taux = T2/taux;
// Comparaison de la température du fusible avec la température de fusion propre au matériel choisi
      when T > mat.Tf then
        etat = false;
      end when;
// Loi d'Ohm
      v = if etat then Ron*i else Roff*i;
      annotation(
        Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}), Text(origin = {1, -1}, extent = {{-101, 41}, {99, -39}}, textString = "%mat"), Text(origin = {0, 69}, textColor = {0, 0, 255}, extent = {{-100, 29}, {100, -29}}, textString = "%name"), Line(origin = {-110, 0}, points = {{-10, 0}, {10, 0}}), Line(origin = {110, 0}, points = {{-10, 0}, {10, 0}}), Rectangle(extent = {{-90, 12}, {90, -12}})}),
        experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-06, Interval = 0.002));
    end FuseN2;
  
  model TestPlusieurs "Banc de test de courants"
    extends Modelica.Icons.Example;
    parameter Integer n = 10 "Nombre de tests desirés";
    Real tab_tdf[n]"Tableau des différents temps de fusion";
    parameter Real tab_Itest[n] = {0.1, 0.5, 1, 2, 3, 4, 5, 10, 20, 50}"Tableau des différents courants de test";
    TestFuseN2 testFuse[n](Itest = tab_Itest) annotation(
      Placement(transformation(extent = {{-16, -16}, {16, 16}})));
  equation
    tab_tdf = testFuse.tempsDeFusion.tempsdefusion;
    annotation(
      experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-06, Interval = 0.02));
  end TestPlusieurs;
  
  
  end Niveau2;

  package Niveau3
  model TestFuseN3
    extends Modelica.Icons.Example;
    parameter Current Itest = 10 "fuse test current";
    Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = 100) annotation(
      Placement(transformation(origin = {0, 46}, extent = {{-10, -10}, {10, 10}})));
    TempsDeFusion tempsDeFusion annotation(
      Placement(transformation(origin = {52, -68}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Basic.Ground ground annotation(
      Placement(transformation(origin = {76, 36}, extent = {{-10, -10}, {10, 10}})));
    Modelica.Electrical.Analog.Basic.Resistor resistor(R = constantVoltage.V/Itest) annotation(
      Placement(transformation(origin = {-48, 6}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
    FuseN3 fuseN3 annotation(
      Placement(transformation(origin = {0, -40}, extent = {{-10, -10}, {10, 10}})));
  equation
    connect(constantVoltage.p, resistor.p) annotation(
      Line(points = {{-10, 46}, {-48, 46}, {-48, 16}}, color = {0, 0, 255}));
    connect(constantVoltage.n, ground.p) annotation(
      Line(points = {{10, 46}, {76, 46}}, color = {0, 0, 255}));
    connect(fuseN3.p, resistor.n) annotation(
      Line(points = {{-10, -40}, {-48, -40}, {-48, -4}}, color = {0, 0, 255}));
    connect(fuseN3.n, ground.p) annotation(
      Line(points = {{10, -40}, {76, -40}, {76, 46}}, color = {0, 0, 255}));
    connect(fuseN3.etat, tempsDeFusion.etat) annotation(
      Line(points = {{0, -30}, {52, -30}, {52, -58}}, color = {255, 0, 255}));
    annotation(
      Diagram,
      experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-6, Interval = 0.02));
  end TestFuseN3;

    model FuseN3 "Fuse model Niveau 3"
      //materiel utilisé
      parameter Materials.MaterialProp mat = Materials.aluminium "fuse material";
      import Modelica.Units.SI.{Voltage,Current,Resistance,Length,Area,Temperature,CoefficientOfHeatTransfer};
      parameter Integer N = 6;
      // Number of spatial grid points
      final parameter Real dx = l/N;
      // Spatial step size
      final parameter Modelica.Units.SI.Length x[N + 1] = {j*dx for j in 0:N} "position";
      constant Temperature T0(displayUnit = "K") = 20 "Temperature de l'air";
      Modelica.Units.SI.Temperature T[N + 1];
      extends Modelica.Electrical.Analog.Interfaces.OnePort;
      // dipole electrique
      // Parametres du fusible
      parameter Length l = 0.03 "Longueur du fil";
      parameter Area S0 = 0.105e-6;
      constant Real u = 0.9;
      Modelica.Units.SI.Area S[N + 1] "Section du fil";
      Resistance Ron[N + 1];
      //Resistance Ron = l/(S*mat.sigma) "Ron";
      //Fusible etat ferme
      //parameter Resistance Roff[N+1]={(j-j+1e5) for j in 0:N} "Roff";
      //Fusible etat ouvert
      parameter Resistance Roff = 1e5;
      Real Sprime[N + 1];
      Modelica.Blocks.Interfaces.BooleanOutput etat annotation(
        Placement(transformation(origin = {2, 98}, extent = {{-10, -10}, {10, 10}}), iconTransformation(origin = {2, 98}, extent = {{-10, -10}, {10, 10}})));
    initial equation
      etat = true;
      T[1] = T0;
      T[2] = T0;
    equation
//x = x+ 1/N;
      for k in 2:N loop
        S[k] = S0*(1 - u*sin(Modelica.Constants.pi*x[k]/l));
        Sprime[k] = der(S[k]);
        Ron[k] = l/(S[k]*mat.sigma) "Ron";
//T[i+1]=2*T[i]-T[i-1]-(((dx)/2)/((der(S)/S)))*(T[i+1]-T[i-1]) -((mat.Res*i^2)/mat.lambda)*((dx)^2)/(S^2);
        T[k + 1] = 2*T[k] - T[k - 1] - (((dx)/2)/(Sprime[k]/S[k]))*(T[k + 1] - T[k - 1]) - ((mat.Res*i^2)/mat.lambda)*((dx)^2)/(S[k]^2);
        when T[k] > mat.Tf then
          etat = false;
        end when;
        v = if etat then Ron[k]*i else Roff*i;
      end for;
      annotation(
        Icon(graphics = {Rectangle(extent = {{-100, 40}, {100, -40}}), Text(origin = {1, -1}, extent = {{-101, 41}, {99, -39}}, textString = "%mat"), Text(origin = {0, 69}, textColor = {0, 0, 255}, extent = {{-100, 29}, {100, -29}}, textString = "%name"), Line(origin = {-110, 0}, points = {{-10, 0}, {10, 0}}), Line(origin = {110, 0}, points = {{-10, 0}, {10, 0}}), Rectangle(extent = {{-90, 12}, {90, -12}})}),
        experiment(StartTime = 0, StopTime = 1, Tolerance = 1e-6, Interval = 0.002));
    end FuseN3;
    model TestPlusieurs "Banc de test de courants"
    extends Modelica.Icons.Example;
    parameter Integer n = 10 "Nombre de tests desirés";
    Real tab_tdf[n]"Tableau des différents temps de fusion";
    parameter Real tab_Itest[n] = {0.1, 0.5, 1, 2, 3, 4, 5, 10, 20, 50} "Tableau des différents courants de test";
    TestFuseN3 testFuse[n](Itest = tab_Itest) annotation(
      Placement(transformation(extent = {{-16, -16}, {16, 16}})));
  equation
    tab_tdf = testFuse.tempsDeFusion.tempsdefusion;
    annotation(
      experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-06, Interval = 0.02));
  end TestPlusieurs;
  
  end Niveau3;

  import Modelica.Units.SI.{Voltage,Current,Resistance,Conductance,Length,Area,Temperature,CoefficientOfHeatTransfer};

  model TempsDeFusion "Counter"
    Real tempsdefusion(start = 0) "Temps de fusion du fusible";
    Real cteTdf(start = 0) "Cte Temps de fusion du fusible";
    Modelica.Blocks.Interfaces.BooleanInput etat annotation(
      Placement(transformation(origin = {2, 96}, extent = {{-20, -20}, {20, 20}}), iconTransformation(origin = {0, 98}, extent = {{-20, -20}, {20, 20}}, rotation = -90)));
  equation
    der(tempsdefusion) = if etat then 1 else 0;
    cteTdf = if not etat then tempsdefusion else 0;
    annotation(
      Icon(graphics = {Ellipse(origin = {1, 0}, extent = {{-71, 70}, {71, -70}}), Ellipse(origin = {1, -1}, extent = {{-63, 61}, {63, -61}}), Rectangle(origin = {0, 17}, extent = {{-4, 33}, {4, -33}}), Rectangle(origin = {11, 0}, extent = {{-21, 4}, {21, -4}}), Ellipse(extent = {{-2, 2}, {2, -2}})}));
  end TempsDeFusion;

  package Materials "library of fuse materials available to the user"
    extends Modelica.Icons.MaterialPropertiesPackage;

    record MaterialProp "base record of material properties"
      extends Modelica.Icons.Record;
      parameter Modelica.Units.SI.Temperature Tf "fusion temperature";
      parameter Modelica.Units.SI.Density rho "density";
      parameter Modelica.Units.SI.Conductivity sigma "conductivité";
      parameter Modelica.Units.SI.SpecificHeatCapacity Cm "capacité thermique massique";
      parameter Modelica.Units.SI.Resistivity Res "Resistivite electrique";
      parameter Modelica.Units.SI.ThermalConductivity lambda " Conductivité thermique";
      annotation(
        Icon(graphics = {Text(origin = {-50, 24}, extent = {{26, -10}, {-26, 10}}, textString = "Tf"), Text(origin = {48, 26}, extent = {{-22, 10}, {22, -10}}, textString = "")}));
    end MaterialProp;

    constant MaterialProp aluminium(Tf = 933, rho = 2700, sigma = 3.5e7, Cm = 897, Res = 2.82e-8, lambda = 237) "Aluminium properties";
    constant MaterialProp silver(Tf = 1235, rho = 10490, sigma = 6.3e7, Cm = 235, Res = 1.59e-8, lambda = 429) "Silver properties";
    constant MaterialProp cuivre(Tf = 1358, rho = 8920, sigma = 5.96e7, Cm = 385, Res = 1.68e-8, lambda = 398) "Cuivre properties";
  end Materials;
  annotation(
    uses(Modelica(version = "4.0.0")));
end ProjectWithMaterial;
